# Movie Recommendation System

This project implements a simple movie recommendation system using Python, pandas and scikit-learn. The system is designed to provide movie recommendations based on user ratings, with an optional user interface in the form of a Command Line Interface (CLI).

## Table of Contents

1. [Project Overview](#project-overview)
2. [Installation](#installation)
3. [Data Cleaning and Analysis](#data-cleaning-and-analysis)
4. [Movie Recommendation Algorithm](#movie-recommendation-algorithm)
5. [User Interface (CLI)](#user-interface-cli)

### Project Overview

The project consists of three main components:

Data Cleaning and Analysis: The provided movie dataset is cleaned to handle irregular or incomplete data. Basic data analysis is performed to uncover insights into user preferences, favorite genres, and average ratings.

Movie Recommendation Algorithm: A recommendation algorithm is developed using collaborative filtering. The algorithm takes into account user ratings and calculates a weighted score for each movie. A fallback system is implemented to recommend popular movies if a user's data doesn't provide enough information.

User Interface (CLI): A simple Command Line Interface is designed to allow users to input their names and receive personalized movie recommendations.

### Installation

To run this project, you need to have Python installed. Additionally, install the required libraries using the following command:

bash
Copy code
pip install pandas scikit-learn surprise

### Data Cleaning and Analysis

The provided dataset is loaded, and data cleaning is performed to handle non-numeric ratings and missing values. Data analysis is conducted to explore user preferences, average ratings, and favorite genres.

### Movie Recommendation Algorithm

A movie recommendation algorithm is developed using collaborative filtering. The algorithm calculates a weighted score for each movie based on user ratings. Incomplete or irregular data is handled, and a fallback system recommends popular movies if user data is insufficient.

### User Interface (CLI)

A simple Command Line Interface (CLI) is designed for users to input their names. The system then provides personalized movie recommendations based on user ratings or suggests popular movies if the user's data is not available.
